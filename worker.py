
import asyncio

from modules import initialize


def main():
    initialize()
    asyncio.get_event_loop().run_forever()

if __name__ == '__main__':
    main()
