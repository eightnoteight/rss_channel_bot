#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

from urllib.parse import urlparse
from datetime import datetime, timedelta
import functools
import os
import asyncio

from aschedule import every
import aiohttp
import aioredis
import feedparser

rss_hosts = [
    ('https://eev.ee/feeds/blog.atom.xml', -1001055245174),
    ('http://hack.limbicmedia.ca/rss/', -1001092590412),
    ('http://www.blog.pythonlibrary.org/feed/', -1001098076228),
    ('https://realpython.com/atom.xml', -1001089149252),
    # ('https://kozikow.com/feed/', ),  # machine learning
    ('http://www.giantflyingsaucer.com/blog/?feed=rss2', -1001081877418),
    ('https://dbader.org/rss', -1001098300684),
    ('https://medium.com/feed/@hwayne/', -1001093877096),
    (os.environ["MY_GITHUB_HOME_FEED"], -1001053300559),  # my github home feed private url
    ('http://stackabuse.com/rss/', -1001067394199),
    ('https://hackernoon.com/feed', -1001071577617),
    ('https://blog.anudeep2011.com/feed/', -1001094867532),
    ('https://culture.zomato.com/feed', -1001081440298),
    ('https://engineering.zomato.com/feed', -1001051204488),
    ('http://turnoff.us/feed.xml', -1001077648174),
    ('https://open.kattis.com/rss/new-problems', -1001087864119),
    ('https://developers.google.com/web/updates/rss.xml', -1001209232540),
]

algorithms_rss = [
    ('https://medium.com/feed/tag/algorithms', -1001122277599),
    ('https://medium.com/feed/tag/competitive-programming', -1001122277599),
    ('https://medium.com/feed/tag/computer-science', -1001122277599),
    ('https://medium.com/feed/tag/cpp', -1001122277599),
    ('https://medium.com/feed/tag/cpp11', -1001122277599),
    ('https://medium.com/feed/tag/data-structures', -1001122277599),
    ('https://medium.com/feed/tag/programming', -1001122277599),
]

machine_learning_rss = [
    ('https://medium.com/feed/tag/big-data', -1001147841681),
    ('https://medium.com/feed/tag/continuous-delivery', -1001147841681),
    ('https://medium.com/feed/tag/continuous-integration', -1001147841681),
    ('https://medium.com/feed/tag/data-science', -1001147841681),
    ('https://medium.com/feed/tag/data-structures', -1001147841681),
    ('https://medium.com/feed/tag/machine-learning', -1001147841681),
    ('https://medium.com/feed/tag/microservices', -1001147841681),
    ('https://medium.com/feed/tag/neural-networks', -1001147841681),
]

software_development_rss = [
    ('https://medium.com/feed/tag/github', -1001144696845),
    ('https://medium.com/feed/tag/javascript', -1001144696845),
    ('https://medium.com/feed/tag/nodejs', -1001144696845),
    ('https://medium.com/feed/tag/npm', -1001144696845),
    ('https://medium.com/feed/tag/react', -1001144696845),
    ('https://medium.com/feed/tag/react-native', -1001144696845),
    ('https://medium.com/feed/tag/react-redux', -1001144696845),
    ('https://medium.com/feed/tag/reactjs', -1001144696845),
    ('https://medium.com/feed/tag/redux', -1001144696845),
    ('https://medium.com/feed/tag/redux-saga', -1001144696845),
    ('https://medium.com/feed/tag/redux-thunk', -1001144696845),
    ('https://medium.com/feed/tag/software-development', -1001144696845),
]

security_rss = [
    ('https://medium.com/feed/tag/crypto-ransomware', -1001132967385),
    ('https://medium.com/feed/tag/cryptography', -1001132967385),
    ('https://medium.com/feed/tag/cyberattack', -1001132967385),
    ('https://medium.com/feed/tag/cybercrime', -1001132967385),
    ('https://medium.com/feed/tag/cybersecurity', -1001132967385),
    ('https://medium.com/feed/tag/encryption', -1001132967385),
    ('https://medium.com/feed/tag/hacking', -1001132967385),
    ('https://medium.com/feed/tag/information-security', -1001132967385),
    ('https://medium.com/feed/tag/infosec', -1001132967385),
    ('https://medium.com/feed/tag/malware', -1001132967385),
    ('https://medium.com/feed/tag/passwords', -1001132967385),
    ('https://medium.com/feed/tag/phishing', -1001132967385),
    ('https://medium.com/feed/tag/privacy', -1001132967385),
    ('https://medium.com/feed/tag/ransomware', -1001132967385),
    ('https://medium.com/feed/tag/security', -1001132967385),
    ('https://medium.com/feed/tag/vpn', -1001132967385),
    ('https://medium.com/feed/tag/wannacry', -1001132967385),
]

blockchain_rss = [
    ('https://medium.com/feed/tag/ethereum', -1001109146140),
]

math_rss = [
    ('https://medium.com/feed/tag/combinatorics', -1001113966417),
    ('https://medium.com/feed/tag/math', -1001113966417),
    ('https://medium.com/feed/tag/number-theory', -1001113966417),
    ('https://medium.com/feed/tag/prime-numbers', -1001113966417),
    ('https://medium.com/feed/tag/statistics', -1001113966417),
    ('https://medium.com/feed/topic/math', -1001113966417),
]

misc_rss = [
]

python_rss = [
    ('https://medium.com/feed/tag/python', -1001132595258),
    ('https://www.reddit.com/r/Python/hot.rss', -1001132595258),
    ('https://medium.com/feed/tag/python-programming', -1001132595258),
    ('https://medium.com/feed/tag/python-web-developer', -1001132595258),
    ('https://medium.com/feed/tag/python3', -1001132595258),
    ('https://medium.com/feed/python-pandemonium', -1001132595258),
]

devops_rss = [
    ('https://medium.com/feed/tag/agile', -1001131563513),
    ('https://medium.com/feed/tag/cloud-computing', -1001131563513),
    ('https://medium.com/feed/tag/devops', -1001131563513),
    ('https://medium.com/feed/tag/docker', -1001131563513),
    ('https://medium.com/feed/tag/kubernetes', -1001131563513),
]

rss_hosts += devops_rss + python_rss + misc_rss + math_rss + blockchain_rss + security_rss + software_development_rss + algorithms_rss + machine_learning_rss



REDIS_URI = urlparse(os.environ['REDIS_URL'])

REDIS_PASSWORD = REDIS_URI.password
REDIS_HOSTNAME = REDIS_URI.hostname
REDIS_PORT = REDIS_URI.port

BOT_URL = os.environ["TELEGRAM_BOT_URL"]

async def scrap_rss(url, channel):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            text = await response.text()
        feed = feedparser.parse(text)
        redis = await aioredis.create_redis(
            (REDIS_HOSTNAME, REDIS_PORT),
            password=REDIS_PASSWORD)
        for entry in feed['entries']:
            if not await redis.sismember(url, entry['link']):
                await redis.sadd(url, entry['link'])
                data = {
                    'chat_id': channel,
                    'text': '[{title}]({link})'.format(title=entry['title'], link=entry['link']),
                    'parse_mode': 'Markdown',
                }
                async with session.get(BOT_URL + 'sendMessage', params=data) as response:
                    print(await response.text())  # logging
        redis.close()


def main():
    for url, channel in rss_hosts:
        # every(functools.partial(scrap_rss, url, channel), hours=1)
        every(functools.partial(scrap_rss, url, channel), hours=1, start_at=datetime.now() + timedelta(minutes=5))

if __name__ == '__main__':
    main()
    asyncio.get_event_loop().run_forever()
